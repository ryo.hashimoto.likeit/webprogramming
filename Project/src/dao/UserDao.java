package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

//データベースを取得するメソッドのテンプレ

public class UserDao {

	public String test() {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE Id = 1";

			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql);

			// 以下SQL文が実行された後の処理を書く
			// rs.next()でレコードを一つ取得できる
			//getString()で、rsの現在行にある指定された列の値を、Stringとして取り出します。
			//String id = rs.getString("id");みたいな

			rs.next();

			String name = rs.getString("name");
			return name;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;

	}//testメソッド

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}//findByInfoメソッド

	public List<User> findAll() {

		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE id not in (1)";

			Statement stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				//ResultSet型.getString("カラム名")で各プロパティを取得→変数に代入

				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);
				//取得したプロパティを入れて、コンストラクタを用いインスタンスを生成

				userList.add(user);
				//最初に作ったAllayList型のuserListに入れていく
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

			// データベース切断
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
		return userList;

	}//findAllメソッド 追加：管理者以外

	public User findDetail(String Id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			//ResultSet型.getString("カラム名")で各プロパティを取得→変数に代入

			User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

			return user;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			// データベース切断
			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}

	}//詳細呼び出しメソッド




	public int newRegi(String RegiId, String RegiName, String RegiBirthDate, String RegiPass) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			String sql = "INSERT INTO user (login_id, name, birth_date, password, create_date, update_date) VALUES (?,?,?,?,now(),now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, RegiId);
			pStmt.setString(2, RegiName);
			pStmt.setString(3, RegiBirthDate);
			pStmt.setString(4, RegiPass);

			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
			//SQLで例外が出た場合、サーブレットのresultに0が代入
		} finally {
			// データベース切断

			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//新規登録メソッド


	public int update(String pass, String username, String birth,String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();


			String sql = "UPDATE user SET password = ?, name = ?, birth_date = ?, update_date = now() WHERE id = ?";


			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, pass);
			pStmt.setString(2, username);
			pStmt.setString(3, birth);
			pStmt.setString(4, id);


			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
			//SQLで例外が出た場合、サーブレットのresultに0が代入
		} finally {
			// データベース切断

			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//更新メソッド



	public int delete(String id) {

		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();


			String sql = "DELETE FROM user WHERE id = ?";


			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);



			int result = pStmt.executeUpdate();

			return result;

		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
			//SQLで例外が出た場合、サーブレットのresultに0が代入
		} finally {
			// データベース切断

			if (conn != null) {
				try {

					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}

	}//削除メソッド

}
