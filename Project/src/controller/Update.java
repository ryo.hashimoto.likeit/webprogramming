package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Update
 */
@WebServlet("/Update")
public class Update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Update() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User session_info = (User) session.getAttribute("userNameId");
		if (session_info == null) {
			response.sendRedirect("Login");
			return;
		}
		//ユーザセッション情報がなかったらログイン画面に遷移

		String id = request.getParameter("id");

		UserDao UserDetail = new UserDao();

		User userDetail = UserDetail.findDetail(id);

		request.setAttribute("userDetail", userDetail);
		request.setAttribute("id", id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String pass = request.getParameter("pass");
		String pass2 = request.getParameter("pass2");
		String username = request.getParameter("username");
		String birth = request.getParameter("birth");
		String loginId = request.getParameter("loginId");
		String id = request.getParameter("id");

		int errNo = 0;

		if (pass.isEmpty() || pass2.isEmpty() || username.isEmpty() || birth.isEmpty()) {
			//すべての項目に入力がされているかどうか
			errNo = 1;

		} else if (!pass.equals(pass2)) {
			//確認パスワードが一致しているかどうか
			errNo = 2;
		} else {
			UserDao userUpdate = new UserDao();

			int addNum = userUpdate.update(pass, username, birth, id);

			if (addNum < 1) {
				errNo = 3;
			}
		}

		switch (errNo) {

		case 1:
			request.setAttribute("errorMassege", "入力された内容は正しくありません（入力漏れ）");
			break;

		case 2:
			request.setAttribute("errorMassege", "入力された内容は正しくありません（パスワード不一致）");
			break;

		case 3:
			request.setAttribute("errorMassege", "入力された内容は正しくありません（SQLが正しく実行されていない）");

			break;
		}

		if (errNo > 0) {

			User user = new User();
			user.setBirthDate(birth);
			user.setName(username);
			user.setLoginId(loginId);

			request.setAttribute("userDetail", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Update.jsp");
			dispatcher.forward(request, response);

		} else {
			response.sendRedirect("Userlist");
		}

	}
}