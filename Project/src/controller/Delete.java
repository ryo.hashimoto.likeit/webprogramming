package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Delete
 */
@WebServlet("/Delete")
public class Delete extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 response.setContentType("text/html; charset=UTF-8");

		 HttpSession session = request.getSession();
		 User session_info = (User)session.getAttribute("userNameId");
		 if(session_info == null) {
			 response.sendRedirect("Login");
			 return;
		 }
		 //ユーザセッション情報がなかったらログイン画面に遷移

			String id = request.getParameter("id");

			UserDao UserDetail = new UserDao();

			User userDetail = UserDetail.findDetail(id);

			request.setAttribute("userDetail",userDetail);
			request.setAttribute("id",id);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");

		UserDao delete = new UserDao();

		int result = delete.delete(id);

		if(result == 1) {
			response.sendRedirect("Userlist");
		}else {
			request.setAttribute("error","削除に失敗しました");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Delete.jsp");
			dispatcher.forward(request, response);
		}
	}

}
