package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Registration() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		 HttpSession session = request.getSession();
		 User session_info = (User)session.getAttribute("userNameId");
		 if(session_info == null) {
			 response.sendRedirect("Login");
			 return;
		 }
		 //ユーザセッション情報がなかったらログイン画面に遷移

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Registration.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String RegiId = request.getParameter("loginId");
		String RegiPass = request.getParameter("pass");
		String RegiPass2 = request.getParameter("pass2");
		String RegiName = request.getParameter("username");
		String RegiBirthDate = request.getParameter("birthDate");

		/*       暗号化
		//ハッシュを生成したい元の文字列
		String source = RegiPass;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String codePass = DatatypeConverter.printHexBinary(bytes);
		*/


		int errNum = 0;

		if(RegiId.isEmpty() || RegiPass.isEmpty() || RegiPass2.isEmpty() || RegiName.isEmpty() || RegiBirthDate.isEmpty()) {
			//すべての項目に入力がされているかどうか
			errNum = 1;

		}else if(!RegiPass.equals(RegiPass2)) {
			//確認パスワードが一致しているかどうか
			errNum = 2;
		}else {
			UserDao userRegistration = new UserDao();

			int addNum = userRegistration.newRegi(RegiId,RegiName,RegiBirthDate,RegiPass);
			//追加されたレコードの数

			if(addNum == 0) {
				errNum = 3;
			}

		}


		switch(errNum) {
			case 1:
				request.setAttribute("errorMassege", "入力された内容は正しくありません（入力漏れ）");
				break;

			case 2:
				request.setAttribute("errorMassege", "入力された内容は正しくありません（確認パスワードが一致しません）");
				break;

			case 3:
				request.setAttribute("errorMassege", "入力された内容は正しくありません（既にそのログインIDは存在します）");
				break;
		}

		if(errNum > 0) {
			User user = new User();
			user.setLoginId(RegiId);
			user.setBirthDate(RegiBirthDate);
			user.setName(RegiName);

			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Registration.jsp");
			dispatcher.forward(request, response);
		}else {
			response.sendRedirect("Userlist");
		}












	}

}
