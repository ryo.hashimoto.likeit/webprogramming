<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page import="javax.servlet.http.HttpServletRequest" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page import="model.User" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/stylesheet2.css">

    <title>ユーザ一覧</title>


  </head>
  <body>

	<header>
		<div class="header-content">
			<div class="text-white d-inline-block mr-5">${userNameId.name}</div>
			<div class="text-right d-inline-block"><a href="Logout" class="text-danger">ログアウト</a></div>
		</div>
	</header>

	<div class="main">

    <h1 class="text-center my-5 text-weight-bold">ユーザ一覧</h1>

    <div class="text-right"><a href="Registration">新規登録</a></div>


		<form>
		<div class="entryform">
			<p>
				<label>ログインID</label> <input type=text name=id>
			</p>


			<p>
				<label>パスワード</label> <input type=password name=pass>
			</p>

			<p>
				<label>生年月日</label> <input type=date name=from id="until"><span></span><input
					type=date name=until id="from">
			</p>
		</div>

			<div class="text-right">
				<input class="mt-4 pull-right w-25" type="submit" value="検索">
			</div>
		</form>



		<hr>








		<table class="table table-bordered">
			<thead>
				<tr>

					<th width="20%">ログインID</th>
					<th width="20%">ユーザ名</th>
					<th width="25%">生年月日</th>
					<th width="35%"></th>
				</tr>
			</thead>
			<tbody>

       <c:forEach var="user" items="${userList}" >

			 		<tr>

						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td>


							<a class="btn btn-primary" href="UserDetail?id=${user.id}">詳細</a>
							<a class="btn btn-success" href="Update?id=${user.id}">更新</a>
							<a class="btn btn-danger" href="Delete?id=${user.id}">削除</a>

						</td>
					</tr>
			  </c:forEach>
			</tbody>
		</table>



	</div>



	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>