<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/stylesheet3.css">

<title>ユーザ情報詳細参照</title>


</head>
<body>

	<header>
		<div class="header-content">
			<div class="text-white d-inline-block mr-5">${userNameId.name}</div>
			<div class="text-right d-inline-block">
				<a href="Logout" class="text-danger">ログアウト</a>
			</div>
		</div>
	</header>

	<div class="main">

		<h1 class="text-center my-5 text-weight-bold">ユーザ情報詳細参照</h1>


		<form>
			<div class="entryform">
				<p>
					<label>ログインID</label> <span>${userDetail.loginId}</span>
				</p>


				<p>
					<label>ユーザ名</label>  <span>${userDetail.name}</span>
				</p>

				<p>
					<label>生年月日</label>  <span>${userDetail.birthDate}</span>
				</p>

				<p>
					<label>登録日時</label> <span>${userDetail.createDate}</span>
				</p>

				<p>
					<label>更新日時</label>  <span>${userDetail.updateDate}</span>
				</p>
			</div>


		</form>

		<a href="Userlist">戻る</a>

	</div>




	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>