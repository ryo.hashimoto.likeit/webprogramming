<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<link rel="stylesheet" href="css/stylesheet3.css">

<title>ユーザ削除確認</title>


</head>
<body>

	<header>
		<div class="header-content">
			<div class="text-white d-inline-block mr-5">${userNameId.name}</div>
			<div class="text-right d-inline-block">
				<a href="Logout" class="text-danger">ログアウト</a>
			</div>
		</div>
	</header>

	<div class="main">

		<h1 class="text-center my-5 text-weight-bold">ユーザ削除確認</h1>
		<h3>${error}</h3>

		<p>ログインID:${userDetail.loginId}</p>
		<p>を本当に削除してもよろしいでしょうか。</p>

		<div class="deleteConfirmation">
			<form action="Userlist">
				<input type="submit" value="キャンセル">
			</form>

			<form action="Delete" method="POST">
				<input type="hidden" name=id value=${id} >
				<input	type="submit" value="OK">
			</form>

		</div>





	</div>




	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>
</body>
</html>