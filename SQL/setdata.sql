-- データベース名 usermanagement

CREATE TABLE user(

	id SERIAL primary key,
	login_id varchar(255) UNIQUE NOT NULL,
	name varchar(255) NOT NULL,
	birth_date DATE NOT NULL,
	password varchar(255) NOT NULL,
	create_date DATETIME NOT NULL,
	update_date DATETIME NOT NULL
	);
	
INSERT INTO user VALUES(
	'1',
	'admin',
	'kanrisya',
	'1993-02-11',
	'password',
	'2019-05-31 15:25:07',
	'2019-05-31 15:25:07'
	);

UPDATE user set name = '管理者' where id = 1;

INSERT INTO user VALUES(
	'2',
	'test',
	'橋本',
	'1993-02-11',
	'ryo211',
	'2019-05-31 15:25:07',
	'2019-05-31 15:25:07'
	);


